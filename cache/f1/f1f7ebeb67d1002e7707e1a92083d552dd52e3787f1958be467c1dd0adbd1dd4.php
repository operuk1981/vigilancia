<?php

/* index.html.twig */
class __TwigTemplate_7c26c49ad601ae5fb2885d5b42643835179028c4103eadd84d8548a653e68654 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"en\"><head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    <link rel=\"icon\" href=\"../../favicon.ico\">

    <title>Datos del edificio</title>

    <!-- Latest compiled and minified CSS -->
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">

<!-- Optional theme -->
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css\">

<!-- Latest compiled and minified JavaScript -->
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js\"></script>    

    
  </head>

  <body>

    <nav class=\"navbar navbar-inverse navbar-fixed-top\">
      <div class=\"container\">
        <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
            <span class=\"sr-only\">Toggle navigation</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
 </button>
          <a class=\"navbar-brand\" href=\"http://localhost/proyecto/index.php\">Ingresos</a>
        </div>
        <div id=\"navbar\" class=\"collapse navbar-collapse\">
          <ul class=\"nav navbar-nav\">
            <li><a href=\"http://localhost/proyecto/registros_ingresos.php\">Egresos</a></li>
            <li><a href=\"http://localhost/proyecto/persona_abm.php\">Persona</a></li>
            <li><a href=\"http://localhost/proyecto/vehiculo_abm.php\">Vehiculo</a></li>
            <li><a href=\"http://localhost/proyecto/quincho_abm.php\">Quincho</a></li>
            <li><a href=\"http://localhost/proyecto/buscador_abm.php\">Buscador</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class=\"container\">
      <form action=\"registrar_ingreso.php\" method=\"POST\" id=\"form\" name=\"form\">

      <div class=\"starter-template\">
        <h1>Plantilla de arranque</h1>
        <p class=\"lead\">REGISTROS DE INGRESOS DE VEHICULOS.</p>
      </div>

      <div class=\"row\">
      \t<div class=\"col-md-4\">
      \t\t<h1>  REGISTRAR INGRESO </h1>      \t\t        

          <p>
           Patente 
          <select name=\"vehiculo\">

          ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["resultados"]) ? $context["resultados"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["vehiculo"]) {
            // line 66
            echo "          <option value=\"";
            echo twig_escape_filter($this->env, $context["vehiculo"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["vehiculo"], "patente", array()), "html", null, true);
            echo "</option>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vehiculo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                    
          
          ?>
            
          </select> 
          </p>
          
          <br/>

          <button type=\"submit\" name=\"guardar\" id=\"guardar\">Registrar</button>
          
      </div>
      </form>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
    <script src=\"../../dist/js/bootstrap.min.js\"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=\"../../assets/js/ie10-viewport-bug-workaround.js\"></script>
  

</body></html>";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 67,  89 => 66,  85 => 65,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index.html.twig", "C:\\xampp\\htdocs\\controlvigilancia\\templates\\index.html.twig");
    }
}
